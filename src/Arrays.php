<?php

namespace MMV\Functions;

class Arrays
{
    /**
     * Sets a value within a deeply nested array using "dot" notation
     *
     * @param array $array
     * @param string $path
     * @param mixed $value
     * @return void
     */
    public static function set($array, $path, $value=null)
    {
        $_path = explode('.', $path);

        $tmp = &$array;

        $n = 1;
        $current = [];
        foreach($_path as $key) {

            $current[] = $key;

            // is index in array?
            if(!array_key_exists($key, $tmp)) {
                // create index and set as []

                $tmp[$key] = [];
            }

            if(count($_path) <= $n) {
                $tmp[$key] = $value;

                unset($tmp);

                return $array;
            } else {
                if(is_array($tmp[$key])) {
                    $tmp = &$tmp[$key];
                } else {
                    throw new self::$_cl_exception ('Variable "'.$path.'" can\'t write because "'.implode('.', $current).'" isn\'t array');
                }
            }

            $n++;
        }

        unset($tmp);

        return $array;
    }

    /**
     * Erase variable within a deeply nested array using "dot" notation
     *
     * @param array $array
     * @param string $path
     * @return void
     */
    public static function erase(array $array, $path)
    {
        $_path = explode('.', $path);

        $tmp = &$array;

        $n = 1;
        $current = [];
        foreach($_path as $key) {

            $current[] = $key;

            if(array_key_exists($key, $tmp)) {
                if(count($_path) == $n) {
                    unset($tmp[$key]);

                    unset($tmp);

                    return $array;
                }

                if(!is_array($tmp[$key])) 
                    throw new self::$_cl_exception ('Variable "'.$path.'" can\'t erase because "'.implode('.', $current).'" isn\'t array');

                $tmp = &$tmp[$key];

                $n++;
            } else {
                unset($tmp);

                return $array;
            }
        }

        unset($tmp);

        return $array;
    }

    /**
     * Retrieves a value from a deeply nested array using "dot" notation
     *
     * @param array $array
     * @param string $path
     * @return mixed
     */
    public static function get(array $array, $path)
    {
        $_path = explode('.', $path);

        $tmp = &$array;

        foreach($_path as $key) {

            if(!is_array($tmp)) {
                unset($tmp);

                return null;
            }

            $tmp = &$tmp[$key];
        }

        $res = $tmp;

        unset($tmp);

        return $res;
    }

    /**
     * Checks that a given item exists in an array using "dot" notation
     *
     * @param array $array
     * @param string $path
     * @return boolean
     */
    public static function has(array $array, $path)
    {
        $_path = explode('.', $path);

        $tmp = &$array;

        foreach($_path as $key) {

            if(!is_array($tmp)) return false;

            if(array_key_exists($key, $tmp)) {
                $tmp = &$tmp[$key];
            } else {
                unset($tmp);
                return false;
            }
        }

        unset($tmp);

        return true;
    }

    ////////////////////////////////////////////////////////////////////////////////

    /**
     * Name class exception
     *
     * @var string
     */
    protected static $_cl_exception = '\Exception';
}
