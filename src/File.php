<?php

namespace MMV\Functions;

class File
{
    /**
     * Flag search files
     *
     * @var integer
     */
    const SEARCH_FILE = 1;

    /**
     * Flag search folder
     *
     * @var integer
     */
    const SEARCH_FOLDER = 2;

    /**
     * Flag search empty folder
     *
     * @var integer
     */
    const SEARCH_FOLDER_EMPTY = 4;

    /**
     * Search files and/or folder
     *
     * Example $filter and $ignore:
     * [
     *     '/.*\.jpg$/i',
     *     '/.*\.gif$/i',
     * ]
     *
     * Flag:
     * - SEARCH_FILE
     * - SEARCH_FOLDER
     * - SEARCH_FOLDER_EMPTY
     *
     * @param string $path
     * @param array $filter
     * @param array ignore
     * @param integer $level
     * @param string $flag
     * @return array
     */
    public static function list($path, $filter=[], $ignore=[], $level=0, $flag=1)
    {
        if((int)$level === 0) {
            $level = 1000000;
        }

        $result = [];
        self::_list(self::normalize($path), $result, $filter, $ignore, $level, $flag);

        return $result;
    }

    /**
     * Realpath to file
     *
     * @param string $path
     * @return string
     */
    public static function realpath($path)
    {
        $res = realpath($path);

        if($res === FALSE) {
            throw new self::$_cl_exception ('File "'.$path.'" not exists');
        }

        return $res;
    }

    public function normalize($path)
    {
        $parts = array(); // Array to build a new path from the good parts
        $path = str_replace('\\', '/', $path); // Replace backslashes with forwardslashes
        $path = preg_replace('/\/+/', '/', $path); // Combine multiple slashes into a single slash
        $segments = explode('/', $path); // Collect path segments
        $test = ''; // Initialize testing variable

        foreach($segments as $segment)
        {
            if($segment != '.')
            {
                $test = array_pop($parts);
                if(is_null($test)) {
                    $parts[] = $segment;
                }
                else if($segment == '..') {
                    if($test == '..') {
                        $parts[] = $test;
                    }

                    if($test == '..' || $test == '') {
                        $parts[] = $segment;
                    }
                }
                else {
                    $parts[] = $test;
                    if($segment) $parts[] = $segment;
                }
            }
        }

        if(!$parts) $parts = ['.'];

        return implode('/', $parts);
    }

    /**
     * Get pathinfo
     *
     * $name:
     * - dirname
     * - basename
     * - extension
     * - filename
     *
     * @param string $path
     * @param string $name
     * @return array
     */
    public static function pathinfo($path, $name='')
    {
        $tmp = $path[strlen($path) - 1];
        if($tmp == '/' || $tmp == '\\') $path = substr($path, 0, -1);

        if(strpos($path, '/') !== false) $separator = '/';
        else $separator = '\\';

        $tmp = explode($separator, $path);
        if(count($tmp) > 1) {
            $basename = end($tmp);
            $dirname = substr($path, 0, strlen($path) - strlen($basename) - 1);
        } else {
            $basename = $path;
            $dirname = '.';
        }

        if (strpos($basename, '.') !== false) {
            $tmp = explode('.', $path);
            $extension = end($tmp);
            $filename = substr($basename, 0, strlen($basename) - strlen($extension) - 1);
        } else {
            $extension = '';
            $filename = $basename;
        }

        $res = array (
            'dirname' => $dirname,
            'basename' => $basename,
            'extension' => $extension,
            'filename' => $filename
        );

        if($name) {
            return $res[$name];
        }

        return $res;
    }

    ////////////////////////////////////////////////////////////////////////////////

    /**
     * Name class exception
     *
     * @var string
     */
    protected static $_cl_exception = '\Exception';

    /**
     * @param string $string
     * @param array $regex
     * @return boolean
     */
    protected static function _filter($string, $regex)
    {
        if(!count($regex)) return true;

        foreach($regex as $pattern) {
            if(preg_match($pattern, $string)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $string
     * @param array $regex
     * @return boolean
     */
    protected static function _ignore($string, $regex)
    {
        if(!count($regex)) return false;

        foreach($regex as $pattern) {
            if(preg_match($pattern, $string)) return true;
        }

        return false;
    }

    /**
     * @param string $path
     * @param array &$result
     * @param array $filter
     * @param array ignore
     * @param integer $level
     * @param integer $flag
     * @return array
     */
    protected static function _list($path, &$result, $filter, $ignore, $level, $flag)
    {
        $res = [];

        $d = dir($path);

        while($entry = $d->read()) {
            $file = $path . '/' . $entry;

            if($entry != '.' && $entry != '..') {
                if(is_dir($file)) {
                    // is dir

                    if($level >= 1) {
                        $tmp = self::_list($file, $result, $filter, $ignore, $level - 1, $flag);
                    }

                    $file .= '/';

                    if($level >= 1) {

                        if(self::SEARCH_FOLDER & $flag && count($tmp)) {
                            if(self::_filter($file, $filter) && !self::_ignore($file, $ignore)) {
                                $result[] = $file; // add folder
                            }
                        }

                        if(self::SEARCH_FOLDER_EMPTY & $flag && !count($tmp)) {
                            if(self::_filter($file, $filter) && !self::_ignore($file, $ignore)) {
                                $result[] = $file; // add folder
                            }
                        }

                    }

                } else {
                    // is file
                    if(self::SEARCH_FILE & $flag && $level >= 1) { // level >= 1
                        if(self::_filter($file, $filter) && !self::_ignore($file, $ignore)) {
                            $result[] = $file; // add file
                        }
                    }
                }

                $res[] = $file;
            }
        }

        $d->close();

        return $res;
    }
}
