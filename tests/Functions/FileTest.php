<?php

namespace MMV\Tests\Functions;

class FileTest extends \PHPUnit_Framework_TestCase
{
    public function getTestClass()
    {
        // create empty folder
        {
            @mkdir($this->getPath().'/empty');
            @mkdir($this->getPath().'/folder1/empty');
            @mkdir($this->getPath().'/folder1/folder2/empty');
        }

        return '\MMV\Functions\File';
    }

    public function getPath()
    {
        return __DIR__.'/data';
    }

    // {{{ Tests

    public function testList_0()
    {
        $class = $this->getTestClass();

        // Test 1

        $res = $class::list($this->getPath(), [], [], 0, 0);

        $expected = [];

        $this->assertEquals($expected, $res);

        // Test 2

        $res = $class::list($this->getPath(), [], [], 1, 0);

        $expected = [];

        $this->assertEquals($expected, $res);
    }

    // get only files
    public function testList_1()
    {
        $class = $this->getTestClass();

        // Test 1

        $res = $class::list($this->getPath(), [], [], 0, 1);

        $expected = [
            __DIR__.'/data/folder1/text2.txt',
            __DIR__.'/data/folder1/folder2/text3.txt',
            __DIR__.'/data/text1.txt',
        ];

        $this->assertEquals($expected, $res);

        // Test 2

        $res = $class::list($this->getPath(), [], [], 1, 1);

        $expected = [
            __DIR__.'/data/text1.txt',
        ];

        $this->assertEquals($expected, $res);
    }

    // get only not empty folder
    public function testList_2()
    {
        $class = $this->getTestClass();

        // Test 1

        $res = $class::list($this->getPath(), [], [], 0, 2);

        $expected = [
            __DIR__.'/data/folder1/folder2/',
            __DIR__.'/data/folder1/',
        ];

        $this->assertEquals($expected, $res);

        // Test 2

        $res = $class::list($this->getPath(), [], [], 1, 2);

        $expected = [
            __DIR__.'/data/folder1/',
        ];

        $this->assertEquals($expected, $res);
    }

    // get only files and not empty folder
    public function testList_3()
    {
        $class = $this->getTestClass();

        // Test 1

        $res = $class::list($this->getPath(), [], [], 0, 3);

        $expected = [
            __DIR__.'/data/folder1/text2.txt',
            __DIR__.'/data/folder1/folder2/text3.txt',
            __DIR__.'/data/folder1/folder2/',
            __DIR__.'/data/folder1/',
            __DIR__.'/data/text1.txt',
        ];

        $this->assertEquals($expected, $res);

        // Test 2

        $res = $class::list($this->getPath(), [], [], 1, 3);

        $expected = [
            __DIR__.'/data/folder1/',
            __DIR__.'/data/text1.txt',
        ];

        $this->assertEquals($expected, $res);
    }

    // get only empty folder
    public function testList_4()
    {
        $class = $this->getTestClass();

        // Test 1

        $res = $class::list($this->getPath(), [], [], 0, 4);

        $expected = [
            __DIR__.'/data/empty/',
            __DIR__.'/data/folder1/empty/',
            __DIR__.'/data/folder1/folder2/empty/',
        ];

        $this->assertEquals($expected, $res);

        // Test 2

        $res = $class::list($this->getPath(), [], [], 1, 4);

        $expected = [
            __DIR__.'/data/empty/',
        ];

        $this->assertEquals($expected, $res);
    }

    // get only empty folder and files
    public function testList_5()
    {
        $class = $this->getTestClass();

        // Test 1

        $res = $class::list($this->getPath(), [], [], 0, 5);

        $expected = [
            __DIR__.'/data/empty/',
            __DIR__.'/data/folder1/text2.txt',
            __DIR__.'/data/folder1/empty/',
            __DIR__.'/data/folder1/folder2/text3.txt',
            __DIR__.'/data/folder1/folder2/empty/',
            __DIR__.'/data/text1.txt',
        ];

        $this->assertEquals($expected, $res);

        // Test 2

        $res = $class::list($this->getPath(), [], [], 1, 5);

        $expected = [
            __DIR__.'/data/empty/',
            __DIR__.'/data/text1.txt',
        ];

        $this->assertEquals($expected, $res);
    }

    // get only folder, empty and not
    public function testList_6()
    {
        $class = $this->getTestClass();

        // Test 1

        $res = $class::list($this->getPath(), [], [], 0, 6);

        $expected = [
            __DIR__.'/data/empty/',
            __DIR__.'/data/folder1/empty/',
            __DIR__.'/data/folder1/folder2/empty/',
            __DIR__.'/data/folder1/folder2/',
            __DIR__.'/data/folder1/',
        ];

        $this->assertEquals($expected, $res);

        // Test 2

        $res = $class::list($this->getPath(), [], [], 1, 6);

        $expected = [
            __DIR__.'/data/empty/',
            __DIR__.'/data/folder1/',
        ];

        $this->assertEquals($expected, $res);
    }

    // get all
    public function testList_7()
    {
        $class = $this->getTestClass();

        // Test 1

        /*
        $res = $class::list($this->getPath(), [], [], 0, 7);

        $expected = [
            __DIR__.'/data/empty/',
            __DIR__.'/data/folder1/text2.txt',
            __DIR__.'/data/folder1/empty/',
            __DIR__.'/data/folder1/folder2/text3.txt',
            __DIR__.'/data/folder1/folder2/empty/',
            __DIR__.'/data/folder1/folder2/',
            __DIR__.'/data/folder1/',
            __DIR__.'/data/text1.txt',
        ];

        $this->assertEquals($expected, $res);
         */

        // Test 2

        $res = $class::list($this->getPath(), [], [], 1, 7);

        $expected = [
            __DIR__.'/data/empty/',
            __DIR__.'/data/folder1/',
            __DIR__.'/data/text1.txt',
        ];

        $this->assertEquals($expected, $res);
    }

    // }}}
}
