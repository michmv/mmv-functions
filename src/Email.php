<?php

namespace MMV\Functions;

class Email
{
    /**
     * @return string
     */
    public function version()
    { // {{{
        return '1.1.1';
    } // }}}

    /**
     * Write address to send
     *
     * @param string $email
     * @param string $name
     * @return this
     */
    public function to($email, $name='')
    { // {{{
        $this->_email['to'] = [$email, $name];

        return $this;
    } // }}}

    /**
     * Write address for copy send email
     *
     * Example:
     * [
     *     'emai1@email.com',
     *     ['email2@email.com', 'Name'],
     * ]
     *
     * @param array $emails
     * @return this
     */
    public function cc($emails)
    { // {{{
        $_emails = [];

        foreach($emails as $email) {
            if(is_array($email)) {
                $email = [$email[0], $email[1]];
            } else {
                $email = [$email, ''];
            }

            $_emails[] = $email;
        }

        $this->_email['cc'] = $_emails;

        return $this;
    } // }}}

    /**
     * Write address for copy send hide email
     *
     * Example:
     * [
     *     'emai1@email.com',
     *     ['email2@email.com', 'Name'],
     * ]
     *
     * @param array $emails
     * @return this
     */
    public function bcc($emails)
    { // {{{
        $_emails = [];

        foreach($emails as $email) {
            if(is_array($email)) {
                $email = [$email[0], $email[1]];
            } else {
                $email = [$email, ''];
            }

            $_emails[] = $email;
        }

        $this->_email['bcc'] = $_emails;

        return $this;
    } // }}}

    /**
     * Addres from
     *
     * @param string $email
     * @param string $name
     * @return this
     */
    public function from($email, $name='')
    { // {{{
        $this->_email['from'] = [$email, $name];

        return $this;
    } // }}}

    /**
     * Addres for reply
     *
     * @param string $email
     * @param string $name
     * @return this
     */
    public function reply($email, $name='')
    { // {{{
        $this->_email['reply'] = [$email, $name];

        return $this;
    } // }}}

    /**
     * Subject for email
     *
     * @param string $text
     * @return this
     */
    public function subject($text)
    { // {{{
        $this->_email['subject'] = $text;

        return $this;
    } // }}}

    /**
     * Body
     *
     * @param string $text
     * @return this
     */
    public function body($text)
    { // {{{
        $this->_email['body'] = $text;

        return $this;
    } // }}}

    /**
     * Set alternative body
     *
     * @param string $text
     * @return this
     */
    public function altBody($text)
    { // {{{
        $this->_email['alt_body'] = $text;

        return $this;
    } // }}}

    /**
     * Add file to message
     *
     * @param stiring $path
     * @param stiring $filename
     * @return this
     */
    public function attach($path, $filename='')
    { // {{{
        $this->_email['attach'][] = [$path, $filename];

        return $this;
    } // }}}

    /**
     * Charset of data. Default UTF-8
     *
     * - utf-8
     * - cp1251
     * - windows-1251
     * - koi8-r
     * - iso-8859-1
     *
     * @param string $encoding
     * @return this
     */
    public function charsetData($encoding)
    { // {{{
        $this->_email['charset_data'] = $encoding;

        return $this;
    } // }}}

    /**
     * Charset for letter. Default UTF-8
     *
     * - utf-8
     * - cp1251
     * - windows-1251
     * - koi8-r
     * - iso-8859-1
     *
     * @param string $encoding
     * @return this
     */
    public function charsetSend($encoding)
    { // {{{
        $this->_email['charset_send'] = $encoding;

        return $this;
    } // }}}

    /**
     * Set type for body
     *
     * @param boolean $is_html
     * @return this
     */
    public function isHtml($is_html=true)
    { // {{{
        if($is_html) {
            $this->_email['content_type'] = 'html';
        } else {
            $this->_email['content_type'] = 'plain';
        }

        return $this;
    } // }}}

    /**
     * Send mail
     *
     * @throw Exception
     * @return boolean
     */
    public function send()
    { // {{{
        $headers = [];

        // to

            if($this->_email['to']) {
                $to = $this->_getAddress('', $this->_email['to'][0], $this->_email['to'][1]);
            } else {
                throw new $this->_cl_exception ('Recipient address unknown.');
            }

        // from

            if($this->_email['from']) {
                $headers[] = $this->_getAddress('From', $this->_email['from'][0], $this->_email['from'][1]);
            }

        // reply

            if($this->_email['reply']) {
                $headers[] = $this->_getAddress('Reply-To', $this->_email['reply'][0], $this->_email['reply'][1]);
            }

        // cc

            if($this->_email['cc']) {
                foreach($this->_email['cc'] as $cc) {
                    $headers[] = $this->_getAddress('Cc', $cc[0], $cc[1]);
                }
            }

        // bcc

            if($this->_email['bcc']) {
                foreach($this->_email['bcc'] as $bcc) {
                    $headers[] = $this->_getAddress('Bcc', $bcc[0], $bcc[1]);
                }
            }

        // subject

            if($this->_email['subject']) {
                $subject = $this->_mime_header_encode($this->_secureHeader($this->_email['subject']));
            } else {
                throw new $this->_cl_exception ('The subject is not filled.');
            }

        // type

            $boundary = md5(uniqid(time()));
            $headers[] = 'Content-Type: multipart/mixed; boundary="b1_'.$boundary.'"';
            $headers[] = "MIME-Version: 1.0";

        // body

            $_content = $this->_readBody($boundary);

            if($this->_isAttach()) {
                foreach($this->_email['attach'] as $file) {
                    $file = $this->_readFile($file);
                    $_content[] = [
                        'Content-Type'=>'application/octet-stream; name="'.$this->_mime_header_encode($file['name']).'"',
                        'Content-Disposition'=>'attachment; filename="'.$this->_mime_header_encode($file['name']).'"',
                        'Content-Transfer-Encoding'=>'base64',
                        'Body'=>$file['body']
                    ];
                }
            }

            $body  = 'This is a multi-part message in MIME format.'."\r\n\r\n";
            $body .= $this->_generateBody($_content, 'b1_'.$boundary);

        // send

            return mail($to, $subject, $body, implode("\r\n", $headers));
    } // }}}

    ////////////////////////////////////////////////////////////////////////////////

    /**
     * Name class exception
     *
     * @var string
     */
    protected $_cl_exception = '\Exception';

    /**
     * @var array
     */
    protected $_email = [
            'charset_data' => 'utf-8',
            'charset_send' => 'utf-8',
            'content_type' => 'plain',
            'to'           => [],
            'from'         => [],
            'reply'        => [],
            'cc'           => [],
            'bcc'          => [],
            'subject'      => '',
            'body'         => '',
            'alt_body'     => '',
            'attach'       => [],
        ];

    /**
     * @var array
     */
    protected $_errors = [];

    /**
     * @param string $type
     * @param string $email
     * @param string $name
     * @return string
     */
    protected function _getAddress($type, $email, $name)
    { // {{{
        if($type) {
            $type = $type . ': ';
        }

        $t = explode(',', $email);
        $email = $t[0]; // only first email
        $email = $this->_secureHeader($email);

        if($name) {
            $address = $this->_mime_header_encode($name) . ' <' . $email . '>';
        } else {
            $address = $email;
        }

        return $type . $address;
    } // }}}

    /**
     * @param string $value
     * @return string
     */
    protected function _secureHeader($value)
    { // {{{
        return trim(preg_replace('/[\r\n]+/', '', $value));
    } // }}}

    /**
     * @param string $str
     * @return string
     */
    protected function _mime_header_encode($str)
    { // {{{
        $charset_data = $this->_email['charset_data'];
        $charset_send = $this->_email['charset_send'];

        if($charset_data != $charset_send) {
            $str = iconv($charset_data, $charset_send, $str);
        }
        return '=?' . $charset_send . '?B?' . base64_encode($str) . '?=';
    } // }}}

    /**
     * Read body
     *
     * @param string $boundary
     * @return string
     */
    protected function _readBody($boundary)
    { // {{{
        if($this->_email['alt_body']) {
            // with alternative body

            $_content = [];

            $_content[] = [
                'Content-Type'=>'text/'.$this->_email['content_type'].'; charset="'.$this->_email['charset_send'].'"',
                'MIME-Version'=>'1.0',
                'Content-Transfer-Encoding'=>'base64',
                'Body'=>$this->_bodyEncode($this->_email['body']),
            ];

            // alternative body

            if($this->_email['content_type'] == 'plain') {
                $type = 'html';
            } else {
                $type = 'plain';
            }

            $_content[] = [
                'Content-Type'=>'text/'.$type.'; charset="'.$this->_email['charset_send'].'"',
                'MIME-Version'=>'1.0',
                'Content-Transfer-Encoding'=>'base64',
                'Body'=>$this->_bodyEncode($this->_email['alt_body']),
            ];

            $_content = [
                'Content-Type'=>'multipart/alternative; boundary="b2_'.$boundary.'"',
                'Body'=>$this->_generateBody($_content, 'b2_'.$boundary),
            ];

        } else {
            // without alternative body

            $_content = [
                'Content-Type'=>'text/'.$this->_email['content_type'].'; charset="'.$this->_email['charset_send'].'"',
                'MIME-Version'=>'1.0',
                'Content-Transfer-Encoding'=>'base64',
                'Body'=>$this->_bodyEncode($this->_email['body']),
            ];
        }

        return [$_content];
    } // }}}

    /**
     * @param string $str
     * @return string
     */
    protected function _bodyEncode($str)
    { // {{{
        $charset_data = $this->_email['charset_data'];
        $charset_send = $this->_email['charset_send'];

        if($charset_data != $charset_send) {
            $str = iconv($charset_data, $charset_send, $str);
        }

        return chunk_split(base64_encode($str));
    } // }}}

    /**
     * Check is attachment files
     *
     * @return boolean
     */
    protected function _isAttach()
    { // {{{
        if(count($this->_email['attach']))
            return true;

        return false;
    } // }}}

    /**
     * @param string $path
     * @return array
     */
    protected function _pathinfo($path)
    { // {{{  
        // TODO удалить эту функцию
        $tmp = $path[strlen($path) - 1];
        if($tmp == '/' || $tmp == '\\') $path = substr($path, 0, -1);

        if(strpos($path, '/') !== false) $separator = '/';
        else $separator = '\\';

        $tmp = explode($separator, $path);
        if(count($tmp) > 1) {
            $basename = end($tmp);
            $dirname = substr($path, 0, strlen($path) - strlen($basename) - 1);
        } else {
            $basename = $path;
            $dirname = '.';
        }

        if (strpos($basename, '.') !== false) {
            $tmp = explode('.', $path);
            $extension = end($tmp);
            $filename = substr($basename, 0, strlen($basename) - strlen($extension) - 1);
        } else {
            $extension = '';
            $filename = $basename;
        }

        return array (
            'dirname' => $dirname,
            'basename' => $basename,
            'extension' => $extension,
            'filename' => $filename
        );
    } // }}}

    /**
     * @param array $file
     * @throw Exception
     * @return array
     */
    protected function _readFile($file)
    { // {{{
        $_path = $file[0];
        $_name = $file[1];

        if(!file_exists($_path)) {
            throw new $this->_cl_exception ('Can\'t attach file "'.$_path.'" because file not found.');
        }

        $fp = fopen($_path, "r");
        $body = fread($fp, filesize($_path));
        fclose($fp);

        if(!$_name) {
            $tmp = $this->_pathinfo($_path);
            $_name = $tmp['basename'];
        }

        if(!$_name) {
            throw new $this->_cl_exception ('Can\'t attach file "'.$_path.'" because filename is empty.');
        }

        return ['name'=>$_name, 'body'=>chunk_split(base64_encode($body))];
    } // }}}

    /**
     * @param array $content
     * @param string $boundary
     * @return string
     */
    protected function _generateBody($content, $boundary)
    { // {{{
        $body = [];

        foreach($content as $block) {
            $tmp = '';
            foreach($block as $key => $val) {
                if($key != 'Body') {
                    $tmp .= $key . ': ' . $val . "\r\n";
                }
            }
            $tmp .= "\r\n" . $block['Body'];

            $body[] = $tmp;
        }

        //$tmp  = 'This is a multi-part message in MIME format.'."\r\n\r\n";
        $tmp = "--".$boundary."\r\n"; // start
        $tmp .= implode("\r\n--".$boundary."\r\n", $body);
        $tmp .= "\r\n--".$boundary."--\r\n"; // finish

        return $tmp;
    } // }}}
}
