<?php

namespace MMV\Functions;

class Strings
{
    /**
     * Explode with more function
     * You can escape separator symbol in string by use "\"
     * Example: for "text1|text2\|text3|text4\\|text5" return ["text1", "text2|text3", "text4\", "text5"]
     *
     * @param string $separator
     * @param string $string
     * @param integer $limit See $limit for explode()
     * @return array
     */
    public static function explode_strong($separator, $string, $limit=null)
    {
        $t = time();

        $_string = self::_quote_symbol_encode($string, $separator, $t);

        if($limit === null)
            $split = explode($separator, $_string);
        else
            $split = explode($separator, $_string, $limit);

        $res = [];

        foreach($split as $i) {
            $res[] = self::_quote_symbol_decode($i, $separator, $t);
        }

        return $res;
    }

    protected static function _quote_symbol_encode($string, $symbol, $mark)
    {
        return str_replace(
            ["\\\\\\$symbol", "\\\\$symbol",         "\\$symbol"],
            ["\\#s1#$mark",      "#s2#$mark$symbol", "#s1#$mark"],
            $string
        );
    }

    protected static function _quote_symbol_decode($string, $symbol, $mark, $modify=true)
    {
        if($modify) {
            return str_replace(
                ["#s2#$mark", "#s1#$mark"],
                ["\\",        $symbol],
                $string
            );
        }

        return str_replace(
            ["#s2#$mark", "#s1#$mark"],
            ["\\\\",      '\\'.$symbol],
            $string
        );
    }

    /**
     * Read content between open tag and close tag and return as array
     * If $do_replace is TRUE to replace tags on meta tag
     * You can use escaping symbol "\" for tags and \
     *
     * @param string $string
     * @param string $open_symbol
     * @param string $close_symbol
     * @param boolean $do_replace
     * @param string $meta_tag
     * @return array
     */
    public static function extract_tags($string, $open_symbol, $close_symbol='', $do_replace=false, $meta_tag='$')
    {
        // init

        $search = $replacements = [];

        $time = time();

        $_open_symbol = preg_quote($open_symbol, '/');

        $_string = self::_quote_symbol_encode($string, $open_symbol, $time); // open tag

        if($close_symbol) {
            $_close_symbol = preg_quote($close_symbol, '/');

            $_string = self::_quote_symbol_encode($_string, $close_symbol, '@'.$time); // close tag

            $second_decode = true;
        } else {
            $close_symbol = $open_symbol; // set close tag from open tag

            $_close_symbol = $_open_symbol;

            $second_decode = false;
        }

        $res = ['found'=>[], 'inside'=>[]];

        // select

        $pattern = "/$_open_symbol(.*?)$_close_symbol/i";

        preg_match_all($pattern, $_string, $matches);

        // create result

        if($do_replace) {
            $res['meta_tag'] = $meta_tag . time();
            $search = $replacements = [];
        }

        foreach($matches[0] as $key => $val) {

            if($do_replace) {
                $index = $res['meta_tag'] . '_' . $key;
            } else {
                $index = $key;
            }

            $tmp = self::_quote_symbol_decode($val, $open_symbol, $time, false);
            if($second_decode) $tmp = self::_quote_symbol_decode($tmp, $close_symbol, '@'.$time, false);
            $res['found'][$index] = $tmp;

            $tmp = self::_quote_symbol_decode($matches[1][$key], $open_symbol, $time);
            if($second_decode) $tmp = self::_quote_symbol_decode($tmp, $close_symbol, '@'.$time);
            $res['inside'][$index] = $tmp;

            if($do_replace) {
                $search[] = $val;
                $replacements[] = $index;
            }
        }

        if($do_replace) {

            $_string = str_replace($search, $replacements, $_string);

            $tmp = self::_quote_symbol_decode($_string, $open_symbol, $time);
            if($second_decode) $tmp = self::_quote_symbol_decode($tmp, $close_symbol, '@'.$time);

            $res['string'] = $tmp;
        }

        return $res;
    }

    /**
     * Quote characters in array
     *
     * @param string $string
     * @param array $symbol
     * @return string
     */
    public static function quote($string, $symbols)
    {
        $search = $replacements = [];

        foreach($symbols as $s) {

            $search[] = '\\'.$s;
            $replacements[] = '\\\\'.$s;

            $search[] = $s;
            $replacements[] = '\\'.$s;

        }

        $_string = str_replace($search, $replacements, $string);

        return $_string;
    }
}
