<?php

namespace MMV\Functions;

use ZipArchive;

class Zip
{
    /**
     * Open zip archive
     *
     * @param string $path
     * @param boolean $new
     * @param boolean $overwrite
     * @throws Exception
     * @return ZipArchive
     */
    public static function open($path, $create=false, $overwrite=false)
    { // {{{
        self::extension_loaded();

        $flag = 0;

        if($create) {
            $flag = $flag | ZipArchive::CREATE;
        }

        if($overwrite) {
            $flag = $flag | ZipArchive::OVERWRITE;
        }

        $zip = new ZipArchive();

        if($zip->open($path, $flag) !== TRUE) {
            throw new self::$_cl_exception ('Can\'t open zip file "'.$path.'"');
        }

        return $zip;
    } // }}}

    /**
     * List files
     *
     * @param ZipArchive
     * @return array
     */
    public static function listFile(ZipArchive $zip)
    { // {{{
        $files = [];

        for($i=0; $i<$zip->numFiles; $i++) {
            $files[] = $zip->getNameIndex($i);
        }

        return $files;
    } // }}}

    /**
     * Add file to archive
     *
     * @param ZipArchive $zip
     * @param string $file
     * @param string $rename
     * @return boolean
     */
    public static function addFile(ZipArchive $zip, $file, $rename='')
    { // {{{
        if($rename) {
            $res = $zip->addFile($file, $rename);
        } else {
            $res = $zip->addFile($file);
        }

        return $res;
    } // }}}

    /**
     * Add empty directory in zip archive
     *
     * @param ZipArchive $zip
     * @param string $dirname
     * @return boolean
     */
    public static function addEmptyDir($zip, $dirname)
    { // {{{
        return $zip->ddEmptyDir($dirname);
    } // }}}

    /**
     * Add string
     *
     * @param ZipArchive $zip
     * @param string $localname
     * @param string $contents
     * @return boolean
     */
    public static function addFromString($localname, $contents)
    { // {{{
        return $zip->addFromString ($localname, $contents);
    } // }}}

    /**
     * Extract file
     *
     * @param ZipArchive $zip
     * @param string $destination
     * @param string|array $files
     * @return boolean
     */
    public static function extractTo(ZipArchive $zip, $destination, $files=[])
    { // {{{
        if(!is_array($files)) {
            $files = [(string)$files];
        }

        if(count($files)) {
            $res = $zip->extractTo($destination, $files);
        }
        else {
            $res = $zip->extractTo($destination); // all
        }

        return $res;
    } // }}}

    /**
     * Close zip archive
     *
     * @param ZipArchive
     */
    public static function close(ZipArchive $zip)
    { // {{{
        return $zip->close();
    } // }}}

    /**
     * Zip info
     *
     * @param string $path
     * @return array
     */
    public static function info($path)
    { // {{{
        self::extension_loaded();

        $zip = new ZipArchive();

        if ($zip->open($path) !== TRUE) {
            throw new self::$_cl_exception ('Can\'t open zip file "'.$path.'"');
        }

        $res = [
            'numFiles' => $zip->numFiles,
            'status' => $zip->status,
            'statusSys' => $zip->statusSys,
            'filename' => $zip->filename,
            'comment' => $zip->comment,
        ];

        $zip->close();

        return $res;
    } // }}}

    /**
     * Check zip file
     *
     * @param string $path
     * @return boolean
     */
    public static function check($path)
    { // {{{
        self::extension_loaded();

        $zip = new ZipArchive();

        $res = $zip->open($path, ZipArchive::CHECKCONS);

        $zip->close();

        if($res !== true) {
            return false;
        }

        return true;
    } // }}}

    ////////////////////////////////////////////////////////////////////////////////

    /**
     * Name class exception
     *
     * @var string
     */
    protected static $_cl_exception = '\Exception';

    /**
     * @throws Exception
     * @return true
     */
    public static function extension_loaded()
    { // {{{
        if(!extension_loaded('zip')) {
            throw new self::$_cl_exception ('Don\'t have ZIP extension');
        }

        return true;
    } // }}}
}
