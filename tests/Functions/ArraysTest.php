<?php

namespace MMV\Tests\Functions;

class ArraysTest extends \PHPUnit_Framework_TestCase
{
    public function getTestClass()
    {
        return '\MMV\Functions\Arrays';
    }

    // {{{

    public function testHas()
    {
        $class = $this->getTestClass();

        $array = ['test'=>null,'sub'=>['test'=>null]];

        $this->assertTrue($class::has($array, 'test'));

        $this->assertFalse($class::has($array, 'test.sub'));

        $this->assertTrue($class::has($array, 'sub.test'));

        $this->assertFalse($class::has($array, 'sub.test.false'));
    }

    public function testGet()
    {
        $class = $this->getTestClass();

        $array = ['test'=>'val_test','sub'=>['subtest'=>'val_subtest']];

        $res = $class::get($array, 'test');

        $this->assertEquals('val_test', $res);

        $res = $class::get($array, 'sub.subtest');

        $this->assertEquals('val_subtest', $res);
    }

    public function testGetError()
    {
        $class = $this->getTestClass();

        $array = ['test'=>'val_test','sub'=>['subtest'=>'val_subtest']];

        $res = $class::get($array, 'sub.ttt.error');

        $this->assertNull($res);

        $res = $class::get($array, 'sub.subtest.error');

        $this->assertNull($res);
    }

    public function testSet()
    {
        $class = $this->getTestClass();

        // Test

        $array = [];

        $array = $class::set ($array, 'test', ['sub'=>'subtest']);

        $this->assertEquals(['test'=>['sub'=>'subtest']], $array);

        // Test

        $array = ['test'=>['sub'=>'text']];

        $array = $class::set ($array, 'test.sub', ['new_array'=>1]);

        $this->assertEquals(['test'=>['sub'=>['new_array'=>1]]] ,$array);
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Variable "test.error" can't write because "test" isn't array
     */
    public function testSetError()
    {
        $class = $this->getTestClass();

        // Test

        $array = ['test'=>'text'];

        $class::set ($array, 'test.error', 'new_value');
    }

    public function testErase()
    {
        $class = $this->getTestClass();

        // Test

        $array = ['test'=>['var'=>'for_delete']];

        $array = $class::erase ($array, 'test.var');

        $this->assertEquals(['test'=>[]], $array);

        // Test

        $array = $class::erase ($array, 'error');

        $this->assertEquals(['test'=>[]], $array);
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Variable "test.error" can't erase because "test" isn't array
     */
    public function testEraseError()
    {
        $class = $this->getTestClass();

        // Test

        $array = ['test'=>'string'];

        $array = $class::erase ($array, 'test.error');
    }

    // }}}
}
