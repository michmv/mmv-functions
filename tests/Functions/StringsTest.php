<?php

namespace MMV\Tests\Functions;

/**
 * $       \$       $       -
 *         \\$      \$      +
 * \$      \\\$     \$      -
 * \\$     \\\\$    \\$     -
 * \\\$    \\\\\$   \\\$    -
 * \\\\\$  \\\\\\$  \\\\$   -
 */

class StringsTest extends \PHPUnit_Framework_TestCase
{
    public function getTestClass()
    {
        return '\MMV\Functions\Strings';
    }

    // {{{ Tests explode_strong

    public function testExplodeStrong_1()
    {
        $class = $this->getTestClass();

        $res = $class::explode_strong('::', '--\::==');

        $this->assertEquals(['--::=='], $res);
    }

    public function testExplodeStrong_2()
    {
        $class = $this->getTestClass();

        $res = $class::explode_strong('::', '--\\\\::==');

        $this->assertEquals(['--\\', '=='], $res);
    }

    public function testExplodeStrong_3()
    {
        $class = $this->getTestClass();

        $res = $class::explode_strong('::', '--\\\\\\::==');

        $this->assertEquals(['--\\::=='], $res);
    }

    public function testExplodeStrong_4()
    {
        $class = $this->getTestClass();

        $res = $class::explode_strong('::', '--\\\\\\\\::==');

        $this->assertEquals(['--\\\\::=='], $res);
    }

    public function testExplodeStrong_5()
    {
        $class = $this->getTestClass();

        $res = $class::explode_strong('::', '--\\\\\\\\\\::==');

        $this->assertEquals(['--\\\\\\::=='], $res);
    }

    public function testExplodeStrong_6()
    {
        $class = $this->getTestClass();

        $res = $class::explode_strong('::', '--::==');

        $this->assertEquals(['--', '=='], $res);
    }

    public function testExplodeStrong_7()
    {
        $class = $this->getTestClass();

        $res = $class::explode_strong('::', '--::==\::==');

        $this->assertEquals(['--', '==::=='], $res);
    }

    // }}}

    // {{{ Tests extract_tags

    public function testExtractTags_0()
    {
        $class = $this->getTestClass();

        $string = '{:test1:}--\{:no\:}--\{:no:}--{:no\:}';

        // Test

        $res = $class::extract_tags($string, '{:', ':}');

        $expected = [
            'found'  => ['{:test1:}'],
            'inside' => ['test1'],
        ];

        $this->assertEquals($expected, $res);

        $this->assertEquals($expected, $res);

        // Test
        
        $time = time();

        $res = $class::extract_tags($string, '{:', ':}', true, 'x');

        $expected = [
            'found'    => ["x{$time}_0" => '{:test1:}'],
            'inside'   => ["x{$time}_0" => 'test1'],
            'meta_tag' => 'x'.$time,
            'string'   => "x{$time}_0--{:no:}--{:no:}--{:no:}",
        ];

        $this->assertEquals($expected, $res);

        $this->assertEquals($expected, $res);
    }

    public function testExtractTags_1()
    {
        $class = $this->getTestClass();

        $res = $class::extract_tags('{:test1:}--{:no2\:}--\{:no3:}--{:t:}--{::}--\\\\{:test4:}--{:test5\\\\:}--\{:no6\:}--\{:no7:}--{:no8\:}', '{:', ':}');

        $expected = [
            'found'=>[
                '{:test1:}',
                '{:no2\:}--\{:no3:}',
                '{:t:}',
                '{::}',
                '{:test4:}',
                '{:test5\\\\:}',
            ],
            'inside'=>[
                'test1',
                'no2:}--{:no3',
                't',
                '',
                'test4',
                'test5\\',
            ],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testExtractTags_2()
    {
        $class = $this->getTestClass();

        $time = time();

        $res = $class::extract_tags('{:test1:}--{:no2\:}--\{:no3:}--{:t:}--{::}--\\\\{:test4:}--{:test5\\\\:}--\{:no6\:}--\{:no7:}--{:no8\:}', '{:', ':}', true, 'xxx');

        $expected = [
            'found'=>[
                'xxx'.$time.'_0' => '{:test1:}',
                'xxx'.$time.'_1' => '{:no2\:}--\{:no3:}',
                'xxx'.$time.'_2' => '{:t:}',
                'xxx'.$time.'_3' => '{::}',
                'xxx'.$time.'_4' => '{:test4:}',
                'xxx'.$time.'_5' => '{:test5\\\\:}',
            ],
            'inside'=>[
                'xxx'.$time.'_0' => 'test1',
                'xxx'.$time.'_1' => 'no2:}--{:no3',
                'xxx'.$time.'_2' => 't',
                'xxx'.$time.'_3' => '',
                'xxx'.$time.'_4' => 'test4',
                'xxx'.$time.'_5' => 'test5\\',
            ],
            'meta_tag'=>'xxx'.time(),
            'string'=>"xxx{$time}_0--xxx{$time}_1--xxx{$time}_2--xxx{$time}_3--\\xxx{$time}_4--xxx{$time}_5--{:no6:}--{:no7:}--{:no8:}",
        ];

        $this->assertEquals($expected, $res);
    }

    public function testExtractTags_3()
    {
        $class = $this->getTestClass();

        $time = time();

        $res = $class::extract_tags('text\\\\text\\\\{:te\\\\st:}', '{:', ':}', true, 'xxx');

        $expected = [
            'found'=>[
                'xxx'.$time.'_0' => '{:te\\\\st:}',
            ],
            'inside'=>[
                'xxx'.$time.'_0' => 'te\\\\st',
            ],
            'meta_tag'=>'xxx'.time(),
            'string'=>"text\\\\text\\xxx{$time}_0",
        ];

        $this->assertEquals($expected, $res);
    }

    public function testExtractTags_4()
    {
        $class = $this->getTestClass();

        $res = $class::extract_tags('{:text1\:}\{:text2:}', '{:', ':}');

        $expected = [
            'found'=>[
                '{:text1\:}\{:text2:}',
            ],
            'inside'=>[
                'text1:}{:text2',
            ],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testExtractTags_5()
    {
        $class = $this->getTestClass();

        $time = time();

        $res = $class::extract_tags('{:text1:}--\{:text1:}', '{:', ':}', true);

        $expected = [
            'found'=>[
                '$'.$time.'_0'=>'{:text1:}'
            ],
            'inside'=>[
                '$'.$time.'_0'=>'text1'
            ],
            'meta_tag'=>'$'.$time,
            'string'=>'$'.$time.'_0--{:text1:}',
        ];

        $this->assertEquals($expected, $res);
    }

    // }}}

    // {{{ Tests quote

    public function testQuote_1()
    {
        $class = $this->getTestClass();

        $res = $class::quote('$', ['$']);

        $this->assertEquals('\$', $res);
    }

    public function testQuote_2()
    {
        $class = $this->getTestClass();

        $res = $class::quote('\$', ['$']);

        $this->assertEquals('\\\\\\$', $res);
    }

    public function testQuote_3()
    {
        $class = $this->getTestClass();

        $res = $class::quote('\\\\$', ['$']);

        $this->assertEquals('\\\\\\\\$', $res);
    }

    public function testQuote_4()
    {
        $class = $this->getTestClass();

        $res = $class::quote('\\\\\\$', ['$']);

        $this->assertEquals('\\\\\\\\\\$', $res);
    }

    // }}}
}
