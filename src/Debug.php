<?php

namespace MMV\Functions;

class Debug
{
    /**
     * Get time of start function
     *
     * @return float
     */
    public static function getTime()
    {
        $start_time = explode(' ', microtime());
        $real_time = $start_time[1] . substr($start_time[0], 1);
        return $real_time;
    }

    /**
     * Difference between time stop and time start
     *
     * @param float $time_stop
     * @param float $time_start
     * @return string
     */
    public static function diffTime($time_stop, $time_start, $scale=8)
    {
        return round($time_stop - $time_start, 8);
    }
}
